$(document).ready(function () {
    $("#addQuestionForm").validate({
        rules: {
            namePoll: {
                required: true,
                minlength: 3,
                maxlength: 255
            },
            question: {
                required: true,
                minlength: 3,
                maxlength: 255
            },
            answer: {
                required: true,
                minlength: 3,
                maxlength: 200
            },
        },
        messages: {
            namePoll: {
                required: "Please enter name poll",
                minlength: "Name poll must be at least 3 characters long",
                maxlength: "Name poll must be less than 255 characters long"
            },
            question: {
                required: "Please enter question",
                minlength: "Question must be at least 3 characters long",
                maxlength: "Question must be less than 255 characters long"
            },
            answer: {
                required: "Please enter answer",
                minlength: "Answer must be at least 3 characters long",
                maxlength: "Answer must be less than 200 characters long"
            }
        },
        submitHandler: function (form) {
            $.get({
                url: "../views/list-poll.html",
                success: function (result) {
                    $(".container").html(result);
                }
            })
        }
    });
    $("#addAnswer").click(function () {
        $(".input-group").append(`
    <br>
    <div class="input-group-append">
        <button style="background-color: #45a0a0" class="btn btn-outline-secondary" type="button" id="addAnswer">
            <i class="fa-solid fa-plus" style="color: #ffffff"></i>
        </button>
    </div>`);
    });
})